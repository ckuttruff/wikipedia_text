class WikiFormatter

  # obviously not comprehensive, but some basic substitutions to transform
  #  wikipeda markup to plain-ish text:
  #  http://en.wikipedia.org/wiki/Help:Wiki_markup

  REGEX_RULES = [# '''Civil War''' => Civil War
                 [/'{2,}/, ''], # bold, italics, etc
                 # <blockquote>foo</blockquote> => foo
                 [/<\/?[^>]+>/, ''], # html tags
                 # [[wikt:anarchism|anarchism]] => anarchism
                 [/\[\[[^|]+\|([^\]]+)\]\]/, '\1'],
                 # [[foo]] => foo
                 [/(\[|\]){2}/, ''],
                 ['&[mn]dash;', '-'],
                 # ...
                ]

  def self.format(str)
    str = CGI.unescape(str).scrub('?')
    REGEX_RULES.each { |r| str.gsub!(r.first, r.last) }
    return str
  end

end
