require 'sequel'

PAGE_START = /^\s*<page>\s*$/
PAGE_END   = /^\s*<\/page>\s*$/

DB = Sequel.connect('postgres://localhost/wikipedia?user=slackz')
DB.create_table?(:articles) do
  primary_key :id
  column :title, 'text', null: false, index: true
  column :page_xml, 'xml', null: false
end

def insert_page(title, xml)
  ds = DB["INSERT INTO articles (title, page_xml) VALUES (?, XMLPARSE(CONTENT ?))", title, xml]
  ds.insert
end

while(s = gets)
  if(s =~ PAGE_START)
    xml = s
    while(s1 = gets)
      title = $1 if s1 =~ /^\s*<title>([^<]*)<\/title>/
      xml << s1
      break if s1 =~ PAGE_END
    end
    insert_page(title, xml)
  end
end
